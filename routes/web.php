<?php

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('/about', 'AboutController@index');
Route::get('/menu', 'MenuController@index');
Route::get('/menu/category/{id}', 'MenuController@category');
Route::get('/brochure', 'BrochureController@index');
Route::get('/location', 'LocationController@index');
