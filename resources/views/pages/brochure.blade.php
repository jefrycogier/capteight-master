@extends('layouts.master')

@section('content')

<div class="content">
    <div class="container">
        <div class="content-title" style="margin-top: 10rem;">

        </div>
        <div class="content-main mb7">
            <div class="row mx-auto my-auto">
                <div id="carousel-team" class="carousel carousel-team slide w-100" data-ride="carousel">
                    <div class="carousel-inner carousel-team-inner w-100" role="listbox">

                        @foreach ($brochure as $row => $value)
                        <?php if ($row == 0) { ?>
                            <?php if ($value->type_file == 'pdf') { ?>
                                <div class="carousel-item  carousel-team-item active">
                                    <div class="col-lg-4 col-md-6">
                                        <a href="{{ $value->file_url }}" target="_blank">
                                            <img src="{{ $value->cover_pdf_url }}" class="figure-img img-fluid z-depth-1" alt="">
                                        </a>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="carousel-item  carousel-team-item active">
                                    <div class="col-lg-4 col-md-6">
                                        <a href="{{ $value->file_url }}" target="_blank">
                                            <img src="{{ $value->file_url }}" class="figure-img img-fluid z-depth-1" alt="">
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } else { ?>
                            <?php if ($value->type_file == 'pdf') { ?>
                                <div class="carousel-item carousel-team-item">
                                    <div class="col-lg-4 col-md-6">
                                        <a href="{{ $value->file_url }}" target="_blank">
                                            <img src="{{ $value->cover_pdf_url }}" class="figure-img img-fluid z-depth-1" alt="">
                                        </a>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="carousel-item carousel-team-item">
                                    <div class="col-lg-4 col-md-6">
                                        <a href="{{ $value->file_url }}" target="_blank">
                                            <img src="{{ $value->file_url }}" class="figure-img img-fluid z-depth-1" alt="">
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                        @endforeach

                    </div>
                    <a class="carousel-control-prev w-auto" href="#carousel-team" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true" style="background-color: black;"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next w-auto" href="#carousel-team" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true" style="background-color: black;"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('jquery')
<script>
    $('#carousel-team').carousel({
        interval: 5000
    })

    $('.carousel-team .carousel-team-item').each(function () {
        var minPerSlide = 4;
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        for (var i = 0; i < minPerSlide; i++) {
            next = next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }

            next.children(':first-child').clone().appendTo($(this));
        }
    });
</script>
@endsection
