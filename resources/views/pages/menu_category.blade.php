@extends('layouts.master')

@section('content')


<div class="content-main bg-header" style="min-height: 100%;margin-top: 100px;">
    <div id="content-main-category">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-4 bg-cyan  center pad15">
                    <figure class="figure" style="vertical-align: middle;">
                        <img id="big_image" src="" class="img-fluid" style="border-radius:50%;">
                        <figcaption id="big_image_caption" class="figure-caption text-center pad15" style="color: #f3e4d1;"></figcaption>
                    </figure>
                </div>
                <div class="col-sm-8 bg-cyan" style="overflow-x: auto">
                    <div class="row">
                        <?php
                        $dark = '<div class="col-3 bg-dark-blue center pad15 view overlay">';
                        $light = '<div class="col-3 bg-light-blue center pad15 view overlay">';
                        ?>
                        @foreach ($menu as $row => $value)

                        <?php if (fmod($row, 4) == 0) { ?>
                            <?php
                            $dTemp = $dark;
                            $dark = $light;
                            $light = $dTemp;
                            ?>
                        <?php } ?>

                        <!-- $num & 1 = ganjil -->
                        <?php if ($row & 1) { ?> 
                            {!! $light !!}
                        <?php } else { ?>
                            {!! $dark !!}
                        <?php } ?>

                        <a onclick='big_image(document.getElementById("img_<?php echo $value->menu_id ?>").src, $("#caption_<?php echo $value->menu_id ?>").html())'>
                            <figure class="figure custom-figure" style="vertical-align: middle;">
                                <img id="img_<?php echo $value->menu_id ?>" src="{{ $value->image_url }}" class="img-fluid" style="border-radius:50%;">
                                <figcaption id="caption_<?php echo $value->menu_id ?>" class="figure-caption text-center pad15" style="color: #f3e4d1;">{{ $value->name }}</figcaption>
                            </figure>
                            <div class="mask waves-effect waves-light rgba-white-slight"></div>
                        </a>
                    </div>

                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
</div>


@endsection


@section('jquery')
<script>
    function big_image(src, name)
    {
        $('#big_image').attr('src', src);
        $('#big_image_caption').html(name);
    }
</script>
@endsection
