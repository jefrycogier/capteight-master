@extends('layouts.master')

@section('content')

<div class="content">
    <div class="container">
        <div class="content-title animate__animated animate__bounceInDown" style="margin-top: 10rem;">
            <h1>about</h1>
        </div>
        <div class="content-main animate__animated animate__bounceInDown" style="text-align: justify">
            {!! $description !!}
        </div>
    </div>
</div>

@endsection


@section('jquery')
<script>

</script>
@endsection
