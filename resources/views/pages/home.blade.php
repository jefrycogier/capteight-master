@extends('layouts.master')

@section('content')

<div class="content" style="min-height: 90%;">

    <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">

        <ol class="carousel-indicators">

            @foreach ($banner as $row => $value)
            <?php if ($row == 0) { ?>
                <li data-target="#carousel-example-1z" data-slide-to="{{ $row }}" class="active"></li>
            <?php } else { ?>
                <li data-target="#carousel-example-1z" data-slide-to="{{ $row }}"></li>
            <?php } ?>
            @endforeach

        </ol>

        <div class="carousel-inner" role="listbox">

            @foreach ($banner as $row => $value)
            <?php if ($row == 0) { ?>
                <div class="carousel-item active">
                    <div class="view" style="background-image: url('{{ $value->image_url }}'); background-repeat: no-repeat; background-size: cover; background-position-x: center; height: 100%;"></div>
                </div>
            <?php } else { ?>
                <div class="carousel-item">
                    <div class="view" style="background-image: url('{{ $value->image_url }}'); background-repeat: no-repeat; background-size: cover; background-position-x: center; height: 100%;"></div>
                </div>

            <?php } ?>
            @endforeach

        </div>

        <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>

    </div>

</div>

@endsection


@section('jquery')
<script>

</script>
@endsection