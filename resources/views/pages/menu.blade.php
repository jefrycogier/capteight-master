@extends('layouts.master')

@section('content')


<div class="content-main">
    <div id="content" style="height: 100%">

        @foreach ($menu_category as $row => $value)
        <?php if ($row == 0) { ?>
            <div id="left" class="hover">
                <a href="/menu/category/{{ $value->menu_category_id }}">
                    <div class="frame">
                        <div class="container">
                            <span class="helper"></span>
                            <figure class="figure animate__animated animate__tada" style="vertical-align: middle;">
                                <img src="{{ $value->image_url }}" class="img-fluid " style="max-height: 13rem;">
                                <figcaption class="figure-caption text-center figure-caption-edit" style="color: #f3e4d1;">{{ $value->name }}</figcaption>
                            </figure>
                        </div>
                    </div>
                </a>
            </div>
        <?php } else { ?>
            <div id="right" class="hover">
                <a href="/menu/category/{{ $value->menu_category_id }}">
                    <div class="frame">
                        <div class="container">
                            <span class="helper"></span>
                            <figure class="figure animate__animated animate__tada" style="vertical-align: middle;">
                                <img src="{{ $value->image_url }}" class="img-fluid" style="max-height: 14rem;transform: rotate(-15deg);">
                                <figcaption class="figure-caption text-center figure-caption-edit" style="color: #f3e4d1;">{{ $value->name }}</figcaption>
                            </figure>
                        </div>
                    </div>
                </a>
            </div>
        <?php } ?>
        @endforeach

    </div>
</div>


@endsection


@section('jquery')
<script>

</script>
@endsection
