@extends('layouts.master')

@section('content')

<div class="content">
    <div class="container">
        <div class="content-title animate__animated animate__bounceInDown" style="margin-top: 10rem;">
            <h1>location</h1>
        </div>
        <div class="content-main animate__animated animate__bounceInDown">
            <div class="container">

                <?php foreach ($contact_arr as $row => $value) { ?>
                    <p class="location-city" style="margin-top: 1rem;">{{ $row }}</p>
                    <?php foreach ($value as $r) { ?>
                        <p class="location-address">{{ $r }}</p>
                    <?php } ?>
                <?php } ?>


            </div>
        </div>
    </div>
</div>

@endsection


@section('jquery')
<script>

</script>
@endsection
