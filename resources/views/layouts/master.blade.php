<?php

use App\Http\Controllers\Controller;
use App\Seo;
use App\SocialMedia;

$googleAnalytic = "https://www.google-analytics.com/analytics.js";
$siteName = "";
$siteDescription = "";
$siteUrl = "";
$siteKeywords = "";
$orgId = Controller::getOrgId();
$org = Controller::getOrg($orgId);
$nameApps = $org->name;
$noWa = "";
$instagram = "#";

if (strlen($orgId) > 0) {
    $modelSeo = Seo::where('org_id', $orgId)->first();
    if ($modelSeo != NULL) {
        $googleAnalytic = $modelSeo->google_analytic;
        $siteName = $modelSeo->site_name;
        $siteDescription = $modelSeo->site_description;
        $siteUrl = $modelSeo->site_url;
        $siteKeywords = $modelSeo->site_keywords;
    }

    $modelSocialMedia = SocialMedia::where('org_id', $orgId)->first();
    if ($modelSocialMedia != NULL) {
        $noWa = $modelSocialMedia->whatsapp;
        $instagram = $modelSocialMedia->instagram_url;
    }
}
?>

<html>
    <head>
        <!--SEO-->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta property="og:site_name" content="{{ $siteName }}">
        <meta property="og:title" content="{{ $siteName }}">
        <meta property="og:description" content="{{ $siteDescription }}">
        <meta property="og:url" content="{{ $siteUrl }}">
        <meta name="keywords" content="{{ $siteKeywords }}">
        <script type="text/javascript" async="" src="{{ $googleAnalytic }}"></script>
        <title>{{ $nameApps }}</title>
        <!--SEO-->

        <!--CSS-->
        <link rel='icon' href='/img/logo.jpg' type='image/x-icon'>
        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/css/mdb.min.css" rel="stylesheet">
        <link href="/css/style.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
        <!--CSS-->

        <!--Font-->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type='text/css'>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
        <!--Font-->


    </head>

    <body>

        <nav class="navbar fixed-top navbar-dark cyan">
            <div class="container">
                <ul class="navbar-nav nav-flex-icons">
                    <li class="nav-item">
                        <a href="{{ $instagram }}" class="nav-link waves-effect waves-light" target="_blank">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="https://api.whatsapp.com/send?phone={{ $noWa }}&text=Halo%21%20Capt%20Eight." class="nav-link waves-effect waves-light" target="_blank">
                            <i class="fab fa-whatsapp"></i>
                        </a>
                    </li>
                </ul>

                <a class="navbar-brand" href="/home">
                    <img src="/img/logo.jpg" height="100" alt="">
                </a>

                <ul class="navbar-nav nav-flex-icons">
                    <li class="nav-item">
                        <div class="burger">
                            <div class="burger__patty"></div>
                            <div class="burger__patty"></div>
                            <div class="burger__patty"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>


        <header class="header">
            <nav class="menu">
                <div class="menu__brand">
                    <a href="/home"><div class="logo"><img src="/img/logo.jpg" alt=""></div></a>
                </div>
                <ul class="menu__list">
                    <li class="menu__item"><a href="/about" class="menu__link">about</a></li>
                    <li class="menu__item"><a href="/menu" class="menu__link">menu</a></li>
                    <li class="menu__item"><a href="/brochure" class="menu__link">brochure</a></li>
                    <li class="menu__item"><a href="/location" class="menu__link">location</a></li>
                </ul>
            </nav>
        </header>

        @yield('content')

        <footer class="page-footer font-small cyan">
            <div class="container">
                <div class="footer-copyright text-left cyan">© 2020 Copyright
                    <a href="#"> Capt Eight Snack & Drink</a>
                </div>
            </div>
        </footer>

        <!--Js-->
        <script type="text/javascript" src="/js/jquery-3.4.1.min.js"></script>
        <script type="text/javascript" src="/js/popper.min.js"></script>
        <script type="text/javascript" src="/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/js/mdb.min.js"></script>
        <script type="text/javascript" src="/js/function.js"></script>
        <!--Js-->

        @yield('jquery')

    </body>


</html>