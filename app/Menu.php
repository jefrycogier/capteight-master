<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model {

    protected $table = "menu";
    protected $primaryKey = 'menu_id';
    protected $fillable = ['org_id', 'menu_category_id', 'name', 'description', 'price', 'image_name', 'image_url', 'created_by', 'created_at', 'updated_by', 'updated_at'];

    public function serviceCategory() {
        return $this->belongsTo('App\MenuCategory', 'menu_category_id');
    }

}
