<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model {

    protected $table = "contact";
    protected $primaryKey = 'contact_id';
    protected $fillable = ['org_id', 'location_id', 'address', 'phone', 'email', 'google_map', 'created_by', 'created_at', 'updated_by', 'updated_at'];

    public function location() {
        return $this->belongsTo(Location::class, 'location_id', 'location_id');
    }

}
