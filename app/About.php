<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model {

    protected $table = "about";
    protected $primaryKey = 'about_id';
    protected $fillable = ['org_id', 'description', 'image_name', 'image_url', 'created_by', 'created_at', 'updated_by', 'updated_at'];

}
