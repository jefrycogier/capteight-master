<?php

namespace App\Http\Controllers;

use App\Brochure;

class BrochureController extends Controller {

    public function __construct() {
        
    }

    public function index() {
        $maintenance = Controller::maintenance();
        if ($maintenance) {
            return view('layouts.maintenance');
        }
        $orgId = Controller::getOrgId();
        $brochure = [];

        if (strlen($orgId) > 0) {
            $brochure = Brochure::where('org_id', $orgId)->get();
        }

        return view('pages.brochure', [
            'brochure' => $brochure,
        ]);
    }

}
