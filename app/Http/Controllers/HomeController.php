<?php

namespace App\Http\Controllers;

use App\Banner;

class HomeController extends Controller {

    public function __construct() {
        
    }

    public function index() {
        $maintenance = Controller::maintenance();
        if ($maintenance) {
            return view('layouts.maintenance');
        }
        $orgId = Controller::getOrgId();
        $banner = [];

        if (strlen($orgId) > 0) {
            $banner = Banner::where('org_id', $orgId)->get();
        }

        return view('pages.home', [
            'banner' => $banner,
        ]);
    }

}
