<?php

namespace App\Http\Controllers;

use App\Menu;
use App\MenuCategory;

class MenuController extends Controller {

    public function __construct() {
        
    }

    public function index() {
        $maintenance = Controller::maintenance();
        if ($maintenance) {
            return view('layouts.maintenance');
        }
        $orgId = Controller::getOrgId();
        $menuCategory = [];

        if (strlen($orgId) > 0) {
            $menuCategory = MenuCategory::where('org_id', $orgId)->get();
        }

        return view('pages.menu', [
            'menu_category' => $menuCategory,
        ]);
    }

    public function category($id) {
        $maintenance = Controller::maintenance();
        if ($maintenance) {
            return view('layouts.maintenance');
        }
        $orgId = Controller::getOrgId();
        $menu = [];

        if (strlen($id) > 0) {
            $menu = Menu::where('menu_category_id', $id)->get();
        }

        return view('pages.menu_category', [
            'menu' => $menu,
        ]);
    }

}
