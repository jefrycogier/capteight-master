<?php

namespace App\Http\Controllers;

use App\Contact;

class LocationController extends Controller {

    public function __construct() {
        
    }

    public function index() {
        $maintenance = Controller::maintenance();
        if ($maintenance) {
            return view('layouts.maintenance');
        }
        $orgId = Controller::getOrgId();
        $contactArr = [];

        if (strlen($orgId) > 0) {
            $contact = Contact::where('org_id', $orgId)->get();

            if (count($contact) > 0) {
                foreach ($contact as $row => $value) {
                    $contactArr[$value->location->name][] = $value->address;
                }
            }
        }

//        var_dump($contactArr);
//        die();

        return view('pages.location', [
            'contact_arr' => $contactArr,
        ]);
    }

}
