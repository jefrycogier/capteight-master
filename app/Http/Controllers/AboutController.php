<?php

namespace App\Http\Controllers;

use App\About;

class AboutController extends Controller {

    public function __construct() {
        
    }

    public function index() {
        $maintenance = Controller::maintenance();
        if ($maintenance) {
            return view('layouts.maintenance');
        }
        $orgId = Controller::getOrgId();
        $image = NULL;
        $description = NULL;

        if (strlen($orgId) > 0) {
            $about = About::where('org_id', $orgId)->first();
            if (isset($about)) {
                $image = $about->image_url;
                $description = $about->description;
            }
        }

        return view('pages.about', [
            'image' => $image,
            'description' => $description,
        ]);
    }

}
